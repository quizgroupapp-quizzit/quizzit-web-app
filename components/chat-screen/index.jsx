import { useRouter } from "next/router";
import React, { useState } from "react";
import HeaderModal from "../header/HeaderModal";
import MenuModal from "../menu/MenuModal";

import ChatModal from "./ChatModal";

const Chat = () => {
  
  const [buttonActive, setButtonActive] = useState("Invidual");
  return (
    <>
      <ChatModal buttonActive={buttonActive} setButtonActive={setButtonActive}>
      </ChatModal>
    </>
  );
};

export default Chat;
