import React from "react";

const Button = ({ children }) => {
  return (
    <button className="flex items-center justify-center rounded-xl bg-blue-700 text-white text-base h-12 w-34 font-medium shadow-sm">
      {children}
    </button>
  );
};

export default Button;
