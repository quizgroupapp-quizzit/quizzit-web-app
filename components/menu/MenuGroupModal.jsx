import React, { useState } from "react";
import { useEffect } from "react";
import classNames from "classnames";
import Group from "../../api/Group";
import GroupCard from "../group-screen/GroupCard";
import { AiOutlinePlusCircle } from "react-icons/ai";

const Menu = () => {
  const [group, setGroup] = useState([]);
  const [ownGroup, setOwnGroup] = useState([]);
  const [joinGroup, setJoinGroup] = useState([]);
  const [isShow, setIsShow] = useState(false);
  // useEffect(async () => {
  //   const param = {
  //     Page: 1,
  //     PageSize: 10,
  //     Sort: "id_asc",
  //     GroupName: "",
  //     StatusId: 0,
  //   };
  //   var response = await Group.getGroup(param);
  //   setGroup(response.data.content);
  //   console.log(group);
  //   const ownGroups = response.data.content.filter(
  //     (item) => 3 === item.currentMemberStatus
  //   );
  //   setOwnGroup(ownGroups);
  //   const joinGroups = response.data.content.filter(
  //     (item) => 3 !== item.currentMemberStatus
  //   );
  //   setJoinGroup(joinGroups);
  // }, []);
  const handleShow = () => {
    console.log(isShow);
    setIsShow(!isShow);
  };
  return (
    <div className="pt-6 absolute overflow-y-hidden hover:overflow-y-scroll bg-white ml-4 h-full">
      <div className="text-black border-b border-gray-400 mx-auto  flex justify-items-center w-11/12 py-2 text-xl">
        Nhóm do bạn quản lý
      </div>
      <div className="p-2 cursor-pointer m-1.5 flex justify-center items-center rounded-lg text-blue-500 bg-blue-100 mx-auto w-72">
        <AiOutlinePlusCircle className="mr-2 text-lg" />
        <span className=" font-bold">Tạo nhóm mới</span>
      </div>
      <div className="mb-2">
        {ownGroup.map((group, key) => {
          return <GroupCard key={key} group={group} />;
        })}
      </div>
      <div>
        <div
          className="text-black border-b border-gray-400 mx-auto flex justify-items-center w-11/12 py-2 text-lg cursor-pointer"
          onClick={handleShow}
        >
          Nhóm do bạn quản lí
        </div>
        <div className={classNames("block", { hidden: !isShow })}>
          <div className="mb-2"></div>
        </div>
      </div>
      <div>
        <div
          className="text-black border-b border-gray-400 mx-auto flex justify-items-center w-11/12 py-2 text-lg cursor-pointer"
          onClick={handleShow}
        >
          Nhóm bạn đã tham gia
        </div>
        <div className={classNames("block", { hidden: !isShow })}>
          <div className="mb-2">
            {joinGroup.map((group, key) => {
              return <GroupCard key={key} group={group} />;
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Menu;
