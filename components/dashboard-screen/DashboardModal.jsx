import React from 'react';
import Share from '../share-header-menu';
const DashboardModal = () => {
    return (
        <Share>
            <div className="h-screen flex items-center justify-center">Dashboard</div>
        </Share>
    );
};
export default DashboardModal;