import React from "react";
import classNames from "classnames";
import { BiCube, BiGroup } from "react-icons/bi";
import { IoChatbubblesOutline } from "react-icons/io5";
import { CgProfile } from "react-icons/cg";
import { FiLogOut } from "react-icons/fi";
import Link from "next/link";
import { useRouter } from "next/router";

const ActiveLink = ({ children, href }) => {
  const router = useRouter();
  return (
    <Link href={href}>
      <div
        className={`${
          router.pathname === href
            ? "flex items-center rounded-xl bg-blue-700 text-white text-xl cursor-pointer h-14 w-72 my-1 p-2 pl-8"
            : "flex items-center rounded-xl bg-white hover:bg-blue-600 text-black hover:text-white text-xl cursor-pointer h-14 w-72 my-1 p-2 pl-8"
        }`}
      >
        {children}
      </div>
    </Link>
  );
};

const MenuModal = () => {
  const route = useRouter();
  const hidden = ["userprofile"];
  const pathName = route.asPath.split("/")[1];

  return (
    <div
      className={classNames(
        "mx-8 py-2 rounded-xl w-80 fixed z-10 bg-white flex flex-col items-center ",
        {
          "mt-32": !hidden.includes(pathName),
          "mt-16": hidden.includes(pathName),
        }
      )}
    >
      <ActiveLink href="/">
        <BiCube className="mr-2" />
        Dashboard
      </ActiveLink>
      <ActiveLink href="/chat/1">
        <IoChatbubblesOutline className="mr-2" />
        Chat
      </ActiveLink>
      <ActiveLink href="/group">
        <BiGroup className="mr-2" />
        Groups
      </ActiveLink>
      <ActiveLink href="/userprofile">
        <CgProfile className="mr-2" />
        UserProfile
      </ActiveLink>
      <Link href="/">
        <div className="flex items-center rounded-xl bg-white hover:bg-red-500 text-black hover:text-white text-xl cursor-pointer h-14 w-72 my-1 p-2 pl-8">
          <FiLogOut className="mr-2" />
          Logout
        </div>
      </Link>
    </div>
  );
};

export default MenuModal;
