import React from "react";

import { AiFillLock } from "react-icons/ai";
import { BsPersonFill } from "react-icons/bs";

const InfoModal = () => {
  return (
    <div className="w-2/5 bg-white rounded-xl p-2 pl-3 pb-5">
      <div className="text-lg font-semibold">Giới thiệu</div>
      <div className="text-lg font-semibold flex items-center">
        <AiFillLock /> Riêng tư
      </div>
      <div className="text-base ml-5">
        Chỉ thành viên mới nhìn thấy mọi người trong nhóm và những gì họ đăng.
      </div>
      <div className="text-lg font-semibold flex items-center">
        <BsPersonFill /> Role
      </div>
      <div className="text-base ml-5">Role</div>
    </div>
  );
};

export default InfoModal;
