import React from "react";
import Image from "next/image";

import { Divider } from "antd";
import ActiveTab from "../util/ActiveTab";
import SearchField from "../util/SearchField";
import { useRouter } from "next/router";

const GroupProfile = () => {
  const router = useRouter();
  const search = router.asPath.slice(
    router.asPath.indexOf("/", 2) + 1,
    router.asPath.length
  );
  return (
    <>
      <div className="flex w-full" style={{ height: "490px" }}>
        <div className="bg-white">
          <div className="flex flex-wrap justify-center items-center">
            <div className="border-2 rounded-lg">
              <Image
                src="/images/logoAppbar.png"
                height="300"
                width="920"
                objectFit="contain"
              />
            </div>
            <div className="w-9/12 text-3xl font-extrabold text-black mt-6">
              Tên group
            </div>
            <div className="w-9/12 text-lg font-medium text-black ">
              Số thành viên
            </div>
          </div>
          <div className="w-9/12 mx-auto">
            <Divider className="bg-gray-300 my-4" />
          </div>
          <div className="flex w-9/12 mx-auto">
            <ActiveTab href="/group/groupID">Giới thiệu</ActiveTab>
            <ActiveTab href="/group/post">Thảo luận</ActiveTab>
            <ActiveTab href="/group/member">Thành viên</ActiveTab>
            <ActiveTab href="/group/quiz">Câu hỏi</ActiveTab>
            <div className="ml-auto">
              <SearchField>{search}</SearchField>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default GroupProfile;
