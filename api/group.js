import axiosClient from "./axiosClient";

const group = {
    getGroup: (param) => {
        const url = 'api/groups';
        return axiosClient.get(url, {params:{...param}});
    }
}

export default group;