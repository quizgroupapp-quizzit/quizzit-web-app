import React from "react";
import { useRecoilState } from "recoil";

import LoginModal from "./LoginModal";

import { userState } from "../../store/userState";
const Login = () => {
  const [, setUserState] = useRecoilState(userState);
  return (
    <>
      <LoginModal setUserState={setUserState} />
    </>
  );
};

export default Login;
