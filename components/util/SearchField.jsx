import React from "react";
import Image from "next/image";
import classNames from "classnames";

const SearchField = ({ children }) => {
  const hidden = ["quiz"];
  return (
    <div
      className={classNames("group flex", {
        hidden: hidden.includes(children),
      })}
    >
      <div className="cursor-pointer shadow-xl rounded-full flex bg-white items-center justify-center h-9 w-9 duration-1500 focus-within:w-80 group-hover:w-80 ">
        <Image
          className=""
          src="/icons/search-status.svg"
          width={25}
          height={25}
          priority="true"
        />
        <input
          placeholder={`Search ${children}`}
          className=" w-0 ml-0 focus:w-3/4 group-hover:w-3/4  focus:ml-4 group-hover:ml-4 h-9 border-none outline-none text-lg duration-2000"
        ></input>
      </div>
    </div>
  );
};

export default SearchField;
