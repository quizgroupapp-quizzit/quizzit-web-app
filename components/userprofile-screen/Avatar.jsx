import React, { useState } from "react";
import Image from "next/image";
import { Upload, message } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

const Avatar = () => {
  const [loading, setLoading] = useState(false);

  const beforeUpload = (file) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    return isJpgOrPng;
  };

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setLoading(true);
      return;
    }
    if (info.file.status === "done") {
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setLoading(false);
      });
      reader.readAsDataURL(info.file.originFileObj);
    }
  };

  return (
    <div className="relative w-24 h-24">
      {!loading ? (
        <Image
          className=" rounded-full "
          // src={userInfo.avatar}
          src="/images/avatar.jpg"
          width={96}
          height={96}
          objectFit={"cover"}
          priority="true"
        />
      ) : (
        <div className="flex justify-center items-center rounded-full border-2 border-gray-300 h-full">
          <LoadingOutlined />
        </div>
      )}
      <Upload
        name="avatar"
        showUploadList={false}
        beforeUpload={beforeUpload}
        onChange={handleChange}
      >
        <div className="rounded-full w-7 h-7 right-0 top-0 absolute bg-black">
          <svg
            className="mx-auto h-full"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="17px"
          >
            <path
              className="fill-current text-gray-200"
              d="M 18.414062 2 C 18.158062 2 17.902031 2.0979687 17.707031 2.2929688 L 15.707031 4.2929688 L 14.292969 5.7070312 L 3 17 L 3 21 L 7 21 L 21.707031 6.2929688 C 22.098031 5.9019687 22.098031 5.2689063 21.707031 4.8789062 L 19.121094 2.2929688 C 18.926094 2.0979687 18.670063 2 18.414062 2 z M 18.414062 4.4140625 L 19.585938 5.5859375 L 18.292969 6.8789062 L 17.121094 5.7070312 L 18.414062 4.4140625 z M 15.707031 7.1210938 L 16.878906 8.2929688 L 6.171875 19 L 5 19 L 5 17.828125 L 15.707031 7.1210938 z"
            />
          </svg>
        </div>
      </Upload>
    </div>
  );
};

export default Avatar;
