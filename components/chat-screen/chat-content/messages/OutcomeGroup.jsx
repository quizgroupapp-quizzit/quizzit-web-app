import React from "react";
import Image from "next/image";

const OutcomeGroup = ({ message, user, lastItem}) => {
  return (
    <div className="w-144 my-8 px-5 flex justify-start">
      <div className="flex">
        <div className="mt-2 mr-4">
          <Image
            className=" rounded-full"
            src={user.avatar}
            width={45}
            height={45}
            objectFit={"cover"}
            priority="true"
          ></Image>
        </div>
        <div>
          <div className="flex justify-between min-w-40">
            <span className="font-bold ">{user.name}</span>
            <span className="text-gray-400 ">{message.createAt}</span>
          </div>
          <div className="py-5 mt-1 px-4 bg-white rounded-tr-2xl rounded-b-2xl max-w-xs float-left inline-block">
            <span>{message.content}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OutcomeGroup;
