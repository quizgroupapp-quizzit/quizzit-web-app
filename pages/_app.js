import "tailwindcss/tailwind.css";
import Head from "next/head";
import "antd/dist/antd.css";
import { RecoilRoot } from "recoil";
import nProgress from "nprogress";

import "../styles/CustomScrollBar.css";
import "../styles/globals.css";
import "nprogress/nprogress.css";

import Auth from "../components/auth/Auth";
import router from "next/router";

router.events.on("routeChangeStart", () => nProgress.start());
router.events.on("routeChangeComplete", () => nProgress.done());
router.events.on("routeChangeError", () => nProgress.done());

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        Quizzit
        <link rel="shortcut icon" href="/static/favicon.ico" />
      </Head>
      <RecoilRoot>
        <Auth>
          <Component {...pageProps} />
        </Auth>
      </RecoilRoot>
    </>
  );
}

export default MyApp;
