import React from "react";
import { FaRegUserCircle } from "react-icons/fa";
import Image from "next/image";

const GroupTitle = ({ group }) => {
  return (
    <div className="h-52 w-80 rounded-2xl border-2 border-gray-700 my-5 ml-10 mr-0 bg-white">
      <Image
        className="rounded-t-xl"
        // src={`${group.image}`}
        src="/images/logoAppbar.png"
        width={320}
        height={142}
        objectFit={"cover"}
      ></Image>
      <div className="flex font-bold justify-between w-full ml-4">
        <span>{group.name}</span>
        <div className="flex items-center mr-8">
          <FaRegUserCircle className="mx-1" />
          <span className="text-blue-500">{group.totalMem}</span>
        </div>
      </div>
    </div>
  );
};

export default GroupTitle;
