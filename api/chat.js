import axiosClient from "./axiosClient";

const chat = {
    getChat: (id)=>{
        const url = `/api/chat/${id}`;
        return axiosClient.get(url,{
            baseURL: 'http://localhost:3001'
        });
    },
    
}
export default chat;