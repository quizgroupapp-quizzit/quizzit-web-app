import React from "react";
import Head from "next/head";
import Userprofile from "../../components/userprofile-screen";
const UserprofilePage = () => {
  return (
    <>
      <Head>
        <title>Quizzit</title>
      </Head>
      <Userprofile></Userprofile>
    </>
  );
};

export default Userprofile;
