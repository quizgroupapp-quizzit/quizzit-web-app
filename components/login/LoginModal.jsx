import React from "react";
import { Form, Input, Button, message } from "antd";
import { useRouter } from "next/router";

import auth from "../../api/auth";

const LoginModal = ({ setUserState }) => {
  const router = useRouter();

  const onFinish = async (values) => {
    message.loading({ key: "login", content: "Logging in" });
    try {
      const token = await auth.login(values);
      setUserState(token);
      router.push("/");
      message.success({ key: "login", content: "success" });
    } catch (err) {
      // console.log(err);
      const error = err.response.data?.title;
      message.error({ key: "login", content: error && "Something went wrong" });
    }
  };

  return (
    <>
      <div className="w-screen h-screen flex justify-center items-center bg">
        <div className="w-1/3 ">
          <Form
            name="basic"
            layout="vertical"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            {/* <Form.Item name="remember" valuePropName="checked">
              <Checkbox>Remember me</Checkbox>
            </Form.Item> */}

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Login
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

export default LoginModal;
