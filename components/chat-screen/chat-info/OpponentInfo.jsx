import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Chat from "../../../api/chat";
import Image from "next/image";
import ContactComponent from "./ContactComponent";
import { BiMailSend } from "react-icons/bi";
import { HiOutlineLocationMarker } from "react-icons/hi";
import { TiPhoneOutline } from "react-icons/ti";

const OpponentInfo = () => {
  const router = useRouter();
  const [user, setUser] = useState();
  const chatId = router.query.index;
  useEffect(async () => {
    var response = await Chat.getChat(chatId);
    setUser(response.data.user);
  }, [chatId]);
  return (
    <div
      className="mt-32 w-72 rounded-3xl bg-white"
      style={{ height: `calc(100% - 190px)` }}
    >
      <div className="mt-10 flex flex-col justify-between items-center">
        {user && (
          <Image
            className=" rounded-full"
            src={user.avatar}
            width={90}
            height={90}
            objectFit={"cover"}
            priority="true"
          ></Image>
        )}
        {user && <span className="mt-3 text-lg font-bold">{user.name}</span>}
        <div className="mt-8">
          <div className="mb-6">
            <span className="font-bold text-xl">Contact Details</span>
          </div>
          <div className="flex flex-col justify-start">
            <ContactComponent title={"Email"} content={"dnn8420@gmail.com"}>
              <BiMailSend />
            </ContactComponent>
            <ContactComponent title={"Phone"} content={"0523093320"}>
              <TiPhoneOutline />
            </ContactComponent>
            <ContactComponent
              title={"Address"}
              content={"TP.Bien Hoa T.DongNai"}
            >
              <HiOutlineLocationMarker />
            </ContactComponent>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OpponentInfo;
