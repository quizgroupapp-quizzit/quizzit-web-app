import React from "react";
import Link from "next/link";

import { useRouter } from "next/router";
import classNames from "classnames";

const ActiveTab = ({ children, href }) => {
  const router = useRouter();
  const path = router.asPath;
  return (
    <Link href={href} shallow={true}>
      <div
        className={classNames(
          "flex items-center justify-center hover:border-b-2 hover:border-blue-700 text-black hover:text-blue-700 text-lg cursor-pointer w-28 h-10 mr-3",
          {
            "border-blue-700 text-blue-700 border-b-2 ": href === path,
          }
        )}
      >
        {children}
      </div>
    </Link>
  );
};

export default ActiveTab;
