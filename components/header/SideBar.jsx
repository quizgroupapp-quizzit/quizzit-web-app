import React from "react";
import Link from "next/link";
import Image from "next/image";

import { AiOutlineBell } from "react-icons/ai";
import { BiGroup } from "react-icons/bi";
import { IoChatbubblesOutline } from "react-icons/io5";
import { CgProfile } from "react-icons/cg";
import { FiLogOut } from "react-icons/fi";
import { RiPieChart2Line } from "react-icons/ri";
import ActiveButton from "../util/ActiveButton";

const SideBar = () => {
  return (
    <>
      <div className="w-16 bg-white bg-opacity-95 h-full flex flex-col justify-between fixed mt-0 z-20">
        <div className="flex  h-14 justify-center items-center">
          <div className="h-14 w-12 mt-6">
            <Link href="/">
              <div>
                <Image
                  className="cursor-pointer"
                  // onClick={onClick}
                  src="/images/logo.ico"
                  width={56}
                  height={56}
                  objectFit="contain"
                  priority="true"
                />
              </div>
            </Link>
          </div>
        </div>
        <div className="flex items-center justify-center flex-col">
          <ActiveButton href="/">
            <RiPieChart2Line />
          </ActiveButton>
          <ActiveButton href="/chat/1">
            <IoChatbubblesOutline />
          </ActiveButton>
          <ActiveButton href="/group/groupID">
            <BiGroup />
          </ActiveButton>
          <ActiveButton href="/userprofile">
            <CgProfile />
          </ActiveButton>
          <ActiveButton href="/logout">
            <FiLogOut />
          </ActiveButton>
        </div>
        <div className="flex flex-col items-center justify-center">
          <div className="h-14 w-11 relative ">
            <Image
              className=" rounded-full pt-3"
              src="/images/avatar.jpg"
              width={40}
              height={40}
              objectFit={"cover"}
              priority="true"
            />
            <div className="rounded-full h-3 w-3 bg-green-500 absolute top-0 right-0 border-2 border-white" />
          </div>
          <AiOutlineBell className="text-2xl w-12 mb-2" />
        </div>
      </div>
    </>
  );
};
export default SideBar;
