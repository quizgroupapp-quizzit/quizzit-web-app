// tailwind.config.js
module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    minWidth: {
      40: "10rem",
    },
    borderRadius: {
      lg: "0.5rem",
      xl: "0.75rem",
      "1.5xl": "0.875rem",
      "2xl": "1rem",
      "2.5xl": "1.25rem",
      "3xl": "1.5rem",
      full: "9999px",
    },
    borderWidth: {
      DEFAULT: "1px",
      0: "0",
      2: "2px",
      3: "3px",
      4: "4px",
      6: "6px",
      8: "8px",
    },
    extend: {
      spacing: {
        18: "4.5rem",
        34: "8.5rem",
        144: "36rem",
      },
      transitionDuration: {
        0: "0ms",
        1500: "1500ms",
        2000: "2000ms",
      },
      fontFamily: {
        lobster: "Lobster",
      },
    },
  },
  variants: {
    extend: {
      width: ["hover", "group-hover", "focus", "focus-within"],
      margin: ["hover", "group-hover", "focus", "focus-within"],
      overflow: ["hover", "focus"],
      borderWidth: ["hover", "focus"],
      display: ["hover", "focus"],
      padding: ["hover", "focus"],
    },
  },
  plugins: [],
};
