import React from "react";
import Image from "next/image";

const IncomeGroup = ({ message, lastItem }) => {
  return (
    <div className="w-144 my-8 px-5 flex justify-end ">
      <div className="flex">
        <div >
          <div className="flex justify-between min-w-40">
            <span className="text-gray-400 ">{message.createAt}</span>
            <span className="font-bold ">Dat Nguyen</span>
          </div>
          <div className="py-5 px-4 bg-white rounded-tl-2xl rounded-b-2xl max-w-xs float-right inline-block mt-1">
            <span>{message.content}</span>
          </div>
        </div>
        <div className="mt-2 ml-4">
          <Image
            className=" rounded-full"
            src="/images/avatar.jpg"
            width={45}
            height={45}
            objectFit={"cover"}
            priority="true"
          ></Image>
        </div>
      </div>
    </div>
  );
};

export default IncomeGroup;
