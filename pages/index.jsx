import Head from "next/head";

import Dashboard from "../components/dashboard-screen";

export default function DashboardPage() {
  return (
    <>
      <Head>
        <title>Quizzit</title>
      </Head>
      <Dashboard />
    </>
  );
}
