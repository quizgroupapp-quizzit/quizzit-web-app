import React from "react";
import Link from "next/link";

import { useRouter } from "next/router";
import classNames from "classnames";

const ActiveButton = ({ children, href }) => {
  const router = useRouter();
  const logoutLink = ["/logout"];
  const path = router.pathname.slice(1, router.pathname.indexOf("/", 2));
  return (
    <Link href={href} shallow={true}>
      <div
        className={classNames(
          "flex items-center justify-center rounded-xl bg-white  text-black hover:text-white text-xl cursor-pointer my-1 w-12 h-12",
          {
            "bg-blue-700 text-white":
              (href.slice(1).startsWith(path) && router.pathname !== "/") ||
              (href === "/" && router.pathname === "/"),
            "hover:bg-blue-600": !logoutLink.includes(href),
            "hover:bg-red-500": logoutLink.includes(href),
          }
        )}
      >
        {children}
      </div>
    </Link>
  );
};

export default ActiveButton;
