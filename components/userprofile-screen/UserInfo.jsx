import React from "react";
import Image from "next/image";
import dynamic from "next/dynamic";

const Pie = dynamic(() => import("@ant-design/charts").then((mod) => mod.Pie), {
  ssr: false,
});

// import { Column } from "@ant-design/charts";

const UserInfo = ({ userInfo }) => {
  const data = [
    { type: "Owner", value: 100 },
    {
      type: "Joined",
      value: 100,
    },
  ];

  const total = data[0].value + data[1].value;

  const config = {
    appendPadding: 10,
    data: data,
    angleField: "value",
    colorField: "type",
    radius: 0.7,
    label: {
      type: "inner",
      offset: "-50%",
      content: ({ value }) => (value * 100) / total,
      style: {
        textAlign: "center",
        fontSize: 14,
      },
    },
    legend: {
      layout: "horizontal",
      position: "bottom",
    },
    interactions: [{ type: "element-active" }],
  };

  return (
    <div className="w-1/4 h-full">
      <div className="h-1/3 flex justify-center items-center flex-col bg-gray-100">
        <div className="h-14 w-14 relative ">
          {/* {userInfo.avatar && (
            <Image
              className=" rounded-full "
              // src={userInfo.avatar}
              src="/images/avatar.jpg"
              width={60}
              height={60}
              objectFit={"cover"}
              priority="true"
            />
          )} */}
          <Image
            className=" rounded-full "
            // src={userInfo.avatar}
            src="/images/avatar.jpg"
            width={60}
            height={60}
            objectFit={"cover"}
            priority="true"
          />
          <div className="rounded-full h-3 w-3 bg-green-500 right-0 top-0 absolute border-2 border-white" />
        </div>
        {/* <div className="flex-wrap">{userInfo.fullName}</div> */}
        <div className="flex-wrap text-lg font-semibold pt-2">Đạt Nguyễn</div>
        <div className="flex-wrap text-sm">longbnh@gmail.com</div>
      </div>
      <div className="h-2/3 bg-gray-200">
        <div>
          <svg
            className="relative bg-gray-100 h-full w-full"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 1416.99 174.01"
          >
            <path
              className="fill-current text-gray-200"
              d="M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z"
              transform="translate(0 -135.53)"
            />
          </svg>
        </div>
        <div className="flex justify-center pt-10">
          <div className="w-full flex">
            <div className="w-1/2 flex flex-col">
              <div className="justify-center flex text-base font-medium opacity-60">
                Owner Group
              </div>
              <div className="justify-center flex text-lg font-semibold opacity-80">
                100
              </div>
            </div>
            <div className="w-1/2 flex flex-col">
              <div className="justify-center flex text-base font-medium opacity-60">
                Joined Group
              </div>
              <div className="justify-center flex text-lg font-semibold opacity-80">
                100
              </div>
            </div>
          </div>
        </div>
        <Pie {...config} />
      </div>
    </div>
  );
};

export default UserInfo;
