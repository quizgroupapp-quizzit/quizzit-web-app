import React, { useState } from "react";
import { Tabs } from "antd";
import UserDetails from "./UserDetails";
import UserPassword from "./UserPassword";

const { TabPane } = Tabs;

const MenuSetting = () => {
  function callback(key) {
    console.log(key);
  }

  return (
    <div className="w-3/4 h-full bg-gray-50 p-16">
      <div className="text-2xl font-bold">Settings</div>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="User Details" key="Details">
          <UserDetails />
        </TabPane>
        <TabPane tab="Change Password" key="Pasword">
          <UserPassword />
        </TabPane>
      </Tabs>
    </div>
  );
};

export default MenuSetting;
