import axiosClient from "./axiosClient";

const contactApi = {
  getContact: () => {
    const url = "/api/chat";
    return axiosClient.get(url, {
        baseURL: 'http://localhost:3001'
    });
  },
  setContact: (params) => {
    const url = "/api/chat";
    return axiosClient.get(url, {
        baseURL: 'http://localhost:3001'
    }); 
  }
};
export default contactApi;
