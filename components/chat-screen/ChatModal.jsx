import React from "react";
import Share from "../share-header-menu";
import ChatMenu from "./chat-menu/ChatMenu";
import ChatContent from "./chat-content/ChatContent";
import OpponentInfo from "./chat-info/OpponentInfo";

const ChatModal = ({ buttonActive, setButtonActive }) => {
  return (
    <Share>
      <div className="h-screen w-full ">
        <div className="h-screen w-full flex mx-auto">
          <div className="mx-auto flex h-screen ">
            <ChatMenu
              buttonActive={buttonActive}
              setButtonActive={setButtonActive}
            />
            <ChatContent></ChatContent>
            <OpponentInfo></OpponentInfo>
          </div>
        </div>
      </div>
    </Share>
  );
};
export default ChatModal;
