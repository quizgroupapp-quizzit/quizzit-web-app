import React from "react";

const ActiveButton = ({ text, buttonActive, setButtonActive }) => {
  const handleClickButton = () => {
    if (text != buttonActive) {
       setButtonActive(text);
    }
  };
  return (
    <div
      className={`${
        text === buttonActive
          ? "h-9 w-4/5 rounded-1.5xl bg-white flex items-center justify-center cursor-pointer mx-2"
          : "h-9 w-4/5 rounded-1.5xl bg-gray-100 flex items-center justify-center cursor-pointer mx-2"
      }`}
      onClick={handleClickButton}
    >
      <span
        className={`${
          text === buttonActive
            ? "text-xl text-blue-700"
            : "text-xl text-gray-400"
        }`}
      >
        {text}
      </span>
    </div>
  );
};
export default ActiveButton;
