import React from "react";
import { FiSend } from "react-icons/fi";
import { ImFilePicture } from "react-icons/im";

const ChatInput = () => {
  return (
    <div className="absolute bottom-5 w-11/12 mx-auto rounded-2xl h-14 bg-white flex justify-between items-center">
      <input
        placeholder="Type your message"
        className="ml-4 w-3/4  h-11/12 border-none outline-none text-lg "
      ></input>
      <div className ="flex items-center justify-between h-full w-20">
        <ImFilePicture className="text-lg text-gray-400 cursor-pointer" />
        <div className=" mr-2 h-10 w-10 rounded-1.5xl flex justify-center items-center text-lg text-white cursor-pointer bg-blue-700">
          <FiSend />
        </div>
      </div>
    </div>
  );
};

export default ChatInput;
