import axiosClient from "./axiosClient";
import fire from "firebase/app";

const auth = {
  login: async (param) => {
    const url = "api/login";
    const customToken = (await axiosClient.post(url, param)).data.customToken;
    const login = await fire.auth().signInWithCustomToken(customToken);
    if (typeof window !== "undefined") {
      const token = await login.user.getIdToken();
      localStorage.setItem("token", token);
      return token;
    }
  },
};

export default auth;
