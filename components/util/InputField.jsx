import React, { useState } from "react";
import classNames from "classnames";

const InputField = ({ label, required, type, ...restProps }) => {
  const [focus, setFocus] = useState(false);
  const [hasValue, setHasValue] = useState(false);
  const handleOnBlur = () => {
    setFocus(false);
  };

  return (
    <div className="relative border border-gray-400 h-14 w-full rounded-lg bg-white">
      <input
        type={type}
        className="border-none mt-6 text-base block w-full outline-none pl-6"
        onFocus={() => setFocus(true)}
        onBlur={handleOnBlur}
        onInput={(e) =>
          e.target.value.length > 0 ? setHasValue(true) : setHasValue(false)
        }
        {...restProps}
      />
      <label
        htmlFor={label}
        className={classNames(
          "absolute flex text-gray-400 left-3 text-2xl select-none pointer-events-none",
          {
            "top-3 transform -translate-y-3/4 py-2 pr-2 pl-1 transition duration-100 ease-in-out ":
              focus,
            "pl-4 top-3": !focus && !hasValue,
            "top-1 pl-4": hasValue && !focus,
          }
        )}
        style={{
          background: focus
            ? "linear-gradient(to top, rgba(255, 255, 255) 52%, transparent 48%)"
            : "",
        }}
      >
        <div
          className={classNames("text-red-500 text-xl", {
            hidden: !required,
            "mt-1": focus,
            "absolute top-1 left-2": !focus,
          })}
        >
          *
        </div>
        {label}
      </label>
    </div>
  );
};

export default InputField;
