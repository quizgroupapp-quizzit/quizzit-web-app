import React from 'react';

const ContactComponent = ({children, title, content}) => {
    return (
        <div className="flex justify-start items-center my-2">
            <div className="rounded-full mr-4 h-10 w-10 bg-gray-200 flex justify-center items-center"><div className="text-2xl text-gray-500">{children}</div></div>
            <div className="flex flex-col justify-start text-base">
                <span>{title}</span>
                <span>{content}</span>
            </div>
        </div>
    );
};

export default ContactComponent;