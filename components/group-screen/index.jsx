import React, { useEffect } from "react";

import GroupProfile from "./GroupProfile";
import MenuGroupModal from "../menu/MenuGroupModal";
import Share from "../share-header-menu";
import { Skeleton } from "antd";
import Discussion from "./group-post/Discussion";

const Groups = () => {
  return (
    <Share>
      <div className="bg-gray-100 h-full w-full relative flex ">
        <MenuGroupModal />
        <div className="flex">
          <div className="w-96" />
          <div className="w-11/12">
            <GroupProfile />
            <div className="w-9/12 mx-auto">
              {/* <Skeleton active /> */}
              <Discussion />
            </div>
          </div>
        </div>
      </div>
    </Share>
  );
};

export default Groups;
