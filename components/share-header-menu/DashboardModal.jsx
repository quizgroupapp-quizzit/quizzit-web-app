import React, { useState } from "react";
import { useEffect } from "react";
import Group from "../../api/Group";
import GroupTitle from "../group-screen/GroupTitle";

const ContentModal = () => {
  //   const [key, setKey] = useState("one");
  const [group, setGroup] = useState([]);
  useEffect(async () => {
    const param = {
      Page: 1,
      PageSize: 10,
      Sort: "id_asc",
      GroupName: "",
      StatusId: 0,
    };
    var response = await Group.getGroup(param);
    console.log(response);
    setGroup(response.data.content);
  }, []);

  return (
    <>
      <div className="flex w-full h-full flex-wrap mt-24 pb-24 ml-96">
        {group.map((group, key) => {
          return <GroupTitle key={key} group={group} />;
        })}
      </div>
    </>
  );
};

export default ContentModal;
