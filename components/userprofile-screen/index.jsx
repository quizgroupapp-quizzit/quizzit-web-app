import React, { useState, useEffect } from "react";
import fire from "firebase/app";
import { useRecoilState } from "recoil";

import Share from "../share-header-menu";
import UserInfo from "./UserInfo";

import user from "../../api/user";
import { userState } from "../../store/userState";
import MenuSetting from "./MenuSetting";

const Userprofile = () => {
  const [userStore] = useRecoilState(userState);
  const [userInfo, setUserInfo] = useState({});

  // useEffect(async () => {
  //   const userId = userStore && userStore.userId;
  //   if (userId !== null) {
  //     try {
  //       const response = await user.getUserById(userId);
  //       setUserInfo(response.data);
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }
  // }, [userStore]);

  return (
    <Share>
      <div className="flex items-center justify-center h-screen">
        <UserInfo userInfo={userInfo} />
        <MenuSetting />
      </div>
    </Share>
  );
};

export default Userprofile;
