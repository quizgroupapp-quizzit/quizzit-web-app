import React, { useState } from "react";
import ChatHeader from "./ChatHeader";
import ChatInput from "./ChatInput";
import Messages from "./messages/Messages";
import { useRouter } from "next/router";
import Chat from "../../../api/chat";
import { useEffect } from "react";

const ChatContent = () => {
  const [lastChatId, setLastChatId] = useState();
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState();
  const router = useRouter();
  const chatId = router.query.index;
  useEffect(async () => {
    if(chatId === lastChatId){
      return ;
    }
    setLastChatId(chatId);
    var response = await Chat.getChat(chatId);
    setUser(response.data.user);
    setMessages(response.data.messages);
  }, [chatId, lastChatId]);
  useEffect(()=>{
    if(chatId !== lastChatId){
      const el = document.getElementById("last-item")
      if(el){
          el.scrollIntoView()
      }
    }
  },[chatId, lastChatId])
  if(!user) return null
  return (
    <div
      className="mx-8 mt-32 relative w-144 bg-gray-100 flex justify-center items-center rounded-3xl"
      style={{ height: `calc(100% - 190px)` }}
    >
      <ChatHeader user={user} />
      <Messages messages={messages} user={user} />
      <ChatInput />
    </div>
  );
};

export default ChatContent;
