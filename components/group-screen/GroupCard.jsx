import React from "react";
import { AiOutlineCalendar } from "react-icons/ai";
import Image from "next/image";

const GroupCard = ({ group }) => {
  return (
    <div className="w-80 cursor-pointer ">
       <div className="bg-white hover:bg-blue-50 flex rounded-xl p-2  w-80 ">
        <Image
          className="rounded-xl"
          src={`${group.image}`}
          width={60}
          height={60}
          objectFit={"cover"}
        ></Image>
        <div className="flex flex-col justify-center ml-2 "> 
          <div className="w-52 overflow-ellipsis whitespace-nowrap overflow-hidden ">
            <span className="font-bold text-lg leading-5 overflow-ellipsis w-5/6 h-7">
              {group.name}
            </span>
          </div>
          <div className="flex items-center">
            <AiOutlineCalendar className="mr-2"/>
            <span>{group.createAt}</span>
          </div> 
        </div>
      </div> 
    </div>
  );
};
export default GroupCard;
