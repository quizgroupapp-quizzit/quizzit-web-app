import firebase from "../../api/firebase";
import fire from "firebase/app";
import { useRouter } from "next/router";
import { useRecoilState } from "recoil";
import { useEffect } from "react";

import { userState } from "../../store/userState";

const Auth = ({ children }) => {
  const [, setUserState] = useRecoilState(userState);
  const router = useRouter();
  firebase;
  useEffect(() => {
    fire.auth().onAuthStateChanged(async (user) => {
      if (user) {
        let token = await user.getIdToken();
        localStorage.setItem("token", token);
        setUserState({ token: token, userId: user.uid });
      } else {
        localStorage.removeItem("token");
        router.push("/dang-nhap");
      }
    });
  }, []);

  return children;
};

export default Auth;
