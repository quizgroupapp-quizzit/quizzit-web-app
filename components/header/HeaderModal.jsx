import React from "react";
import Link from "next/link";
import Image from "next/image";

import { AiOutlineBell } from "react-icons/ai";
import { BiGroup } from "react-icons/bi";
import { IoChatbubblesOutline } from "react-icons/io5";
import { Divider, Menu, Dropdown } from "antd";
import { CgProfile } from "react-icons/cg";
import { FiLogOut } from "react-icons/fi";

import ActiveButton from "../util/ActiveButton";

const menu = (
  <Menu>
    <Menu.Item>
      <ActiveButton href="/userprofile">
        <CgProfile className="mr-2" />
        UserProfile
      </ActiveButton>
    </Menu.Item>
    <Menu.Item>
      <Link href="/">
        <div className="flex items-center justify-center rounded-xl bg-white hover:bg-red-500 text-black hover:text-white text-xl cursor-pointer h-14 w-44">
          <FiLogOut className="mr-2" />
          Logout
        </div>
      </Link>
    </Menu.Item>
  </Menu>
);

const HeaderModal = () => {
  // const onClick = (e) =>{
  //   setView("Main");
  // };
  return (
    <>
      <div className="w-full bg-white bg-opacity-95 h-24 flex items-center justify-between fixed mt-0 z-20">
        <div className="flex items-center w-3/4">
          <div className="h-16 mr-8 w-80 relative">
            <Link href="/">
              <div>
                <Image
                  className="cursor-pointer"
                  // onClick={onClick}
                  src="/images/logoAppbar.png"
                  layout="fill"
                  objectFit="contain"
                  priority="true"
                />
              </div>
            </Link>
          </div>
        </div>
        <div className="flex items-center">
          <ActiveButton href="/chat/1">
            <IoChatbubblesOutline className="mr-2" />
            Chat
          </ActiveButton>
          <Divider type="vertical" />
          <ActiveButton href="/group">
            <BiGroup className="mr-2" />
            Groups
          </ActiveButton>

          <AiOutlineBell className="mx-8 text-2xl" />
          <Dropdown overlay={menu} placement="bottomLeft" arrow>
            <div className="h-14 w-14 mr-8  relative ">
              <Image
                className=" rounded-full "
                src="/images/avatar.jpg"
                width={60}
                height={60}
                objectFit={"cover"}
                priority="true"
              />
              <div className="rounded-full h-6 w-6 bg-green-500 absolute left-9 top-9 border-2 border-white" />
            </div>
          </Dropdown>
        </div>
      </div>
    </>
  );
};
export default HeaderModal;
