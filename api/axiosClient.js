import axios from "axios";
import queryString from "query-string";

const axiosClient = axios.create({
  baseURL: process.env.API_URL,
  headers: {
    Authorization: "",
    "content-type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

//Do something before request is send
axiosClient.interceptors.request.use(async (config) => {
  // Handle token here ...
  // console.log(firebase.getUser());
  const token = localStorage.getItem("token");
  config.headers.Authorization = `Bearer ${token}`;

  return config;
});

//Do something with response data
axiosClient.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // Handle errors
    throw error;
  }
);
export default axiosClient;
