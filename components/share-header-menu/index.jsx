import React from "react";

import HeaderModal from "../header/HeaderModal";
// import MenuModal from "../menu/MenuModal";
import SideBar from "../header/SideBar";

const Share = ({ children }) => {
  return (
    <>
      <div className="flex h-full w-full bg-gray-50 font-lobster">
        {/* <HeaderModal /> */}
        <SideBar />
        <div className="ml-12 w-full h-screen">{children}</div>
      </div>
    </>
  );
};

export default Share;
