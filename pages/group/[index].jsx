import React from "react";
import Head from "next/head";
import Groups from "../../components/group-screen";

const GroupPage = () => {
  return (
    <>
      <Head>Quizzit</Head>
      <Groups />
    </>
  );
};

export default GroupPage;
