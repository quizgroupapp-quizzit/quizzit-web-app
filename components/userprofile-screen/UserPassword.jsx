import React from "react";

import { Form } from "antd";
import Button from "../util/Button";
import InputField from "../util/InputField";

const UserPassword = () => {
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <>
      <div className="w-full ml-6">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 18,
          }}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            required={false}
            label=" "
            name="password"
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <InputField
              key="password"
              label="Password"
              type="password"
              required="true"
            />
          </Form.Item>

          <Form.Item
            required={false}
            label=" "
            name="rePassword"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || value === getFieldValue("password"))
                    return Promise.resolve();
                  return Promise.reject(new Error("Re-password not match"));
                },
              }),
            ]}
          >
            <InputField
              key="rePassword"
              label="Re-Password"
              type="password"
              required="true"
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 7,
              span: 16,
            }}
          >
            <Button>Submit</Button>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default UserPassword;
