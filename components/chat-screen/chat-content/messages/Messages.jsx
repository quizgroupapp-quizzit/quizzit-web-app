import React from "react";
import MessageRender from "./MessageRender";


const Message = ({ messages, user }) => {
  
  return (
    <div
      className=" w-full absolute top-18 overflow-y-hidden overflow-x-hidden hover:overflow-y-scroll"
      style={{ height: `calc(100% - 148px)` }}
    >
      {messages &&
        messages.map((message, index) => {
          const lastItem = index === messages.length - 1;
          return <MessageRender 
                      message={message} 
                      user={user} 
                      lastItem={lastItem}
                      key={index}/>;
        })}
    </div>
  );
};

export default Message;
