import React from "react";
import Head from "next/head";
import Chat from "../../components/chat-screen";

const ChatPage = () => {
  return (
    <>
      <Head>Quizzit</Head>
      <Chat />
    </>
  );
};

export default ChatPage;
