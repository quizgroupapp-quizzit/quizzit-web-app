import React from "react";
import InfoModal from "./InfoModal";
import Post from "./Post";

const Discussion = () => {
  return (
    <div className="flex mt-3">
      <Post />
      <InfoModal />
    </div>
  );
};

export default Discussion;
