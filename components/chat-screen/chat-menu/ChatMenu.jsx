import React, { useState } from "react";
import { HiPlus } from "react-icons/hi";
import { useEffect } from "react";
import ActiveButton from "./ActiveButton";
import ChatTab from "./ChatTab";
import Contact from "../../../api/contact";

const ChatMenu = ({ buttonActive, setButtonActive }) => {
  const [invidual, setInvidual] = useState([]);
  const [group, setGroup] = useState([]);
  useEffect(async () =>{
    var response = await Contact.getContact()
    setInvidual(response.data)
  },[])
  return (
    <div
      className="mt-32 w-72 rounded-3xl bg-white"
      style={{ height: `calc(100% - 190px)` }}
    >
      <div className="w-4/5 flex items-center justify-between mx-auto mt-5">
        <span className="text-2xl ">Chat</span>
        <div className=" w-16 h-12 flex items-center justify-center bg-blue-700 rounded-2.5xl">
          <HiPlus className="text-white text-xl" />
        </div>
      </div>
      <div className="w-5/6 h-14 mt-4 mb-5 flex items-center justify-between m-auto rounded-2.5xl bg-gray-100">
        <ActiveButton
          text="Invidual"
          buttonActive={buttonActive}
          setButtonActive={setButtonActive}
        />
        <ActiveButton
          text="Groups"
          buttonActive={buttonActive}
          setButtonActive={setButtonActive}
        />
      </div>
      {buttonActive === "Invidual" ? (
        <div
          className="overflow-y-hidden overflow-x-hidden hover:overflow-y-scroll "
          style={{ height: `calc(100% - 180px)` }}
        >
          {invidual && invidual.map((invidual) => {
            return <ChatTab invidual={invidual}  />;
          })}
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default ChatMenu;
