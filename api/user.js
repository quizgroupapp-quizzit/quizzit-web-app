import axiosClient from "./axiosClient";

const user = {
  getUserById: (id) => {
    const url = `/api/users/${id}`;
    return axiosClient.get(url);
  },
};

export default user;
