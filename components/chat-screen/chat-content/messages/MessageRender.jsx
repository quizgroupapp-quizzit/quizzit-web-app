import React from "react";
import IncomeGroup from "./IncomeGroup";
import OutcomeGroup from "./OutcomeGroup";
import { useEffect } from "react";

const MessageRender = ({ message, lastItem, user }) => {
  useEffect(() => {
      if(lastItem) {
          const el = document.getElementById("last-item")
          if(el){
              el.scrollIntoView()
          }
      }
  }, [lastItem]);
  return (
    <div id={lastItem && "last-item"}>
      {message.type === "income" ? (
        <IncomeGroup message={message} />
      ) : (
        <OutcomeGroup message={message} user={user} />
      )}
    </div>
  );
};

export default MessageRender;
