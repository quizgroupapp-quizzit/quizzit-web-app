import React, { useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import Link from "next/link";

const ChatTab = ({ invidual }) => {
  const router = useRouter();
  const chatId = router.query.index;
  const active = chatId !== invidual.href;
  
  return (
    <Link href={invidual.href}>
      <div
        className={`${
          active
            ? "w-72 h-28 cursor-pointer border-b border-gray-200 flex flex-col justify-between bg-white hover:bg-gray-200"
            : "w-72 h-28 cursor-pointer border-b border-gray-200 flex flex-col justify-between bg-gray-200"
        }`}
      >
        <div className="flex flex-row h-full w-72 my-4 justify-between">
          <div className=" w-full flex flex-row my-2 justify-between">
            <div className="flex flex-col justify-between items-center px-4 w-full ">
              <div className="flex justify-between items-center w-52 ">
                <div className="w-7 h-7 flex items-center">
                  <Image
                    className=" rounded-full"
                    src={invidual.user.avatar}
                    width={28}
                    height={28}
                    objectFit={"cover"}
                    priority="true"
                  ></Image>
                </div>
                <span className="font-bold pl-2 text-lg overflow-ellipsis whitespace-nowrap overflow-hidden w-48">
                  {invidual.user.name}
                </span>
              </div>
              <span className="text-gray-400  overflow-ellipsis whitespace-nowrap overflow-hidden w-52">
                {invidual.messages[invidual.messages.length-1].content}
              </span>
            </div>
            <div className="flex flex-col justify-between items-center w-full ">
              {invidual.lastPresent !== 0 ? (
                <span className={`${active ? "text-gray-400" : "font-bold"}`}>
                  {invidual.lastPresent}m
                </span>
              ) : (
                <div className="rounded-full w-4 h-4 bg-green-500 "></div>
              )}
              {invidual.notification !== 0 ? (
                <div className="rounded-full bg-pink-500 flex justify-center items-center w-6 h-6">
                  <span className="text-white">{invidual.notification}</span>
                </div>
              ) : (
                <div></div>
              )}
            </div>
          </div>
          <div
            className={`${
              active ? "w-1" : "h-full rounded-full w-1 bg-blue-600"
            }`}
          ></div>
        </div>
      </div>
    </Link>
  );
};
export default ChatTab;
