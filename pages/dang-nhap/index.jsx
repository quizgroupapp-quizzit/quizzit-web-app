import React from "react";
import Head from "next/head";
import Login from "../../components/login";

const LoginPage = () => {
  return (
    <>
      <Head>Quizzit</Head>
      <Login />
    </>
  );
};

export default LoginPage;
