import React from "react";

import Avatar from "./Avatar";
import Button from "../util/Button";

const UserDetails = () => {
  return (
    <>
      <Avatar />
      <Button>Save</Button>
    </>
  );
};

export default UserDetails;
