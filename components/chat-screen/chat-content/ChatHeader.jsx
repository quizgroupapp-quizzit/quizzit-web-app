import React from "react";
import Image from "next/image";
import { TiPhoneOutline } from "react-icons/ti";
import { HiOutlineVideoCamera } from "react-icons/hi";
import { FiInfo } from "react-icons/fi";

const ChatHeader = ({ user }) => {
  return (
    <div className="flex justify-between items-center  w-full h-18 rounded-t-3xl absolute top-0 bg-white bg-opacity-95">
      <div className="flex justify-between items-center w-52 ml-5">
        {user && (
          <Image
            className=" rounded-full"
            src={user.avatar}
            width={30}
            height={30}
            objectFit={"cover"}
            priority="true"
          ></Image>
        )}
        {user &&
          <span className="font-bold pl-2 text-lg overflow-ellipsis whitespace-nowrap overflow-hidden w-48">
            {user.name}
          </span>
        }
      </div>
      <div className="rounded-2.5xl w-34 h-11 bg-gray-100 mr-5">
        <div className=" h-full w-4/5 flex items-center justify-between mx-auto text-xl text-gray-400">
          <TiPhoneOutline className="cursor-pointer" />
          <HiOutlineVideoCamera className="cursor-pointer" />
          <FiInfo className="cursor-pointer" />
        </div>
      </div>
    </div>
  );
};

export default ChatHeader;
