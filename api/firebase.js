import fire from "firebase/app";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyCc7KTwJYshnR7GxMAD6NLsmIBI_zLG8es",
  authDomain: "groupsharingblogadminauth.firebaseapp.com",
  projectId: "groupsharingblogadminauth",
  storageBucket: "groupsharingblogadminauth.appspot.com",
  messagingSenderId: "218080389510",
  appId: "1:218080389510:web:daee0694c2540b5631a407",
  measurementId: "G-H81L5ZREP4",
};

const firebase = async () => {
  if (!fire.apps.length) {
    fire.initializeApp(config);
  } else {
    fire.app(); // if already initialized, use that one
  }
  await fire.auth().setPersistence(fire.auth.Auth.Persistence.LOCAL);
};

export default firebase();

// class Firebase {
//   constructor() {
//     if (!fire.apps.length) {
//       fire.initializeApp(config);
//     } else {
//       fire.app(); // if already initialized, use that one
//     }
//     this.auth = fire.auth();
//   }
//   async login(token) {
//     await this.auth.setPersistence(fire.auth.Auth.Persistence.SESSION);
//     return await this.auth.signInWithCustomToken(token);
//   }
//   getUser() {
//     return this.auth.currentUser;
//   }
//   async checkLogin() {
//     return this.auth.onAuthStateChanged();
//   }
//   async logout() {
//     localStorage.removeItem("user");
//     return await this.auth.signOut();
//   }
// }

// export default new Firebase();
